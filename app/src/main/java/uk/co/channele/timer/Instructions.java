/* 
 * @(#) AboutApp.java
 * 
 * Copyright 2013 to 2018 Steven J Lilley
 *
 * It provides a simple dialog fragment giving app/licence details.
 *
 * This file is part of PenaltyTimer. 
 *
 * PenaltyTimer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * stevenjlilley@gmail.com
 */
package uk.co.channele.timer;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

public class Instructions extends DialogFragment {

	@SuppressWarnings("UnusedParameters")
	public static Instructions newInstance(int id) {
		return new Instructions();
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		Dialog instructionsDialog;
		View view = inflater.inflate(R.layout.instructions, null);
		builder.setView(view);
		builder.setNeutralButton(R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				Instructions.this.getDialog().cancel();
			}
		});
		instructionsDialog = builder.create();
		instructionsDialog.setTitle(getString(R.string.instructionsTitle));
		return instructionsDialog;
	}

}
