package uk.co.channele.timer;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.Looper;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import static uk.co.channele.timer.Player.*;
import static uk.co.channele.timer.PlayerView.*;
import static uk.co.channele.timer.TimerViewModel.*;

import java.util.Arrays;

public class PenaltyTimer extends AppCompatActivity implements View.OnClickListener, View.OnLongClickListener, ColourPicker.SettingsListener {

    @SuppressWarnings("unused")
    private static final String TAG = "PT";
    private SharedPreferences appPrefs;
    private Vibrator vibe;
    private VibrationEffect vibeShort;
    private VibrationEffect vibeLong;
    private TimerViewModel model;
    // UI variables
    private Button masterHold;
    private final Button[] buttons = new Button[8];
    private final Button[] addPenaltyButtons = new Button[8];
    private final Button[] subPenaltyButtons = new Button[8];
    private Handler timer;
    private final Updater updater = new Updater();
    private ColourPicker colourPicker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        model = ViewModelProvider.NewInstanceFactory.getInstance().create(TimerViewModel.class);
        if (model.getTeamName() == null) {
            model.setTeamName(getString(R.string.team));
            model.setAltTeamName(getString(R.string.altTeam));
            model.setColours(getResources().getStringArray(R.array.colours));
        }
        appPrefs = getPreferences(MODE_PRIVATE);
        model.retrievePreferences(appPrefs);
        switch (model.getCurrentView()) {
            case JAMMERS:
                setContentView(R.layout.jammers);
                break;
            case BLOCKERS:
                setContentView(R.layout.blockers);
                int colour = model.getTeamBackgroundColor();
                findViewById(R.id.team).setBackgroundColor(colour);
                break;
            default:
                setContentView(R.layout.main);
                break;
        }
        getButtonsForView();
        setTeamDetails();
        vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibeShort = VibrationEffect.createOneShot(100L, VibrationEffect.DEFAULT_AMPLITUDE);
        vibeLong = VibrationEffect.createOneShot(200L, VibrationEffect.DEFAULT_AMPLITUDE);
        timer = new Handler(Looper.getMainLooper());
        if (savedInstanceState == null) {
            checkBattery();
        }

    }

    /**
     * Add or subtract a penalty to/from the given timer.
     *
     * @param player     the player
     * @param adjustment must be +1 or -1
     */
    private void adjustPenalty(Player player, int adjustment) {
        switch (adjustment) {
            case (1):
                model.getTimer(player).addPenalty();
                break;
            case (-1):
                model.getTimer(player).subtractPenalty();
                break;
        }
        setPlayerButtonState(player);
        setPenaltyAdjustButtonPairState(player);
    }

    /**
     * Perform common tasks on a blocker button click.
     *
     * @param blocker the blocker button id
     */
    private void blockerButtonTasks(Player blocker) {
        if (!model.getTimer(blocker).isRunning() || model.getTimer(blocker).isHeld()) {
            model.getTimer(blocker).start();
            setPenaltyAdjustButtonPairState(blocker);
        }
    }

    /**
     * Check that the battery level is reasonable and warn if not.
     */
    private void checkBattery() {
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatusIntent = this.registerReceiver(null, intentFilter);
        if (batteryStatusIntent != null) {
            int status = batteryStatusIntent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            if (status == BatteryManager.BATTERY_STATUS_DISCHARGING ||
                    status == BatteryManager.BATTERY_STATUS_NOT_CHARGING) {
                int batLevel = batteryStatusIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                int maxLevel = batteryStatusIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
                int batPercent = batLevel * 100 / maxLevel;
                if (batPercent < 20) {
                    Toast.makeText(this, getString(R.string.lowBatteryMessage), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    /**
     * Get the buttons from the view. These vary across the three available views.
     * (moved from onCreate)
     */
    private void getButtonsForView() {
        if (model.getCurrentView() == ALL) {
            buttons[ALT_BLOCKER_1.ordinal()] = findViewById(R.id.altBlocker1);
            buttons[ALT_BLOCKER_2.ordinal()] = findViewById(R.id.altBlocker2);
            buttons[ALT_BLOCKER_3.ordinal()] = findViewById(R.id.altBlocker3);
            addPenaltyButtons[ALT_BLOCKER_1.ordinal()] = findViewById(R.id.altBlocker1Add);
            addPenaltyButtons[ALT_BLOCKER_2.ordinal()] = findViewById(R.id.altBlocker2Add);
            addPenaltyButtons[ALT_BLOCKER_3.ordinal()] = findViewById(R.id.altBlocker3Add);
            subPenaltyButtons[ALT_BLOCKER_1.ordinal()] = findViewById(R.id.altBlocker1Sub);
            subPenaltyButtons[ALT_BLOCKER_2.ordinal()] = findViewById(R.id.altBlocker2Sub);
            subPenaltyButtons[ALT_BLOCKER_3.ordinal()] = findViewById(R.id.altBlocker3Sub);
        }
        if (model.getCurrentView() != BLOCKERS) {
            buttons[JAMMER.ordinal()] = findViewById(R.id.jammer);
            buttons[ALT_JAMMER.ordinal()] = findViewById(R.id.altJammer);
            addPenaltyButtons[JAMMER.ordinal()] = findViewById(R.id.jammerAdd);
            addPenaltyButtons[ALT_JAMMER.ordinal()] = findViewById(R.id.altJammerAdd);
            subPenaltyButtons[JAMMER.ordinal()] = findViewById(R.id.jammerSub);
            subPenaltyButtons[ALT_JAMMER.ordinal()] = findViewById(R.id.altJammerSub);
        }
        if (model.getCurrentView() != JAMMERS) {
            buttons[BLOCKER_1.ordinal()] = findViewById(R.id.blocker1);
            buttons[BLOCKER_2.ordinal()] = findViewById(R.id.blocker2);
            buttons[BLOCKER_3.ordinal()] = findViewById(R.id.blocker3);
            addPenaltyButtons[BLOCKER_1.ordinal()] = findViewById(R.id.blocker1Add);
            addPenaltyButtons[BLOCKER_2.ordinal()] = findViewById(R.id.blocker2Add);
            addPenaltyButtons[BLOCKER_3.ordinal()] = findViewById(R.id.blocker3Add);
            subPenaltyButtons[BLOCKER_1.ordinal()] = findViewById(R.id.blocker1Sub);
            subPenaltyButtons[BLOCKER_2.ordinal()] = findViewById(R.id.blocker2Sub);
            subPenaltyButtons[BLOCKER_3.ordinal()] = findViewById(R.id.blocker3Sub);
        }
        for (int i = 0; i < buttons.length; i++) {
            if (buttons[i] != null) {
                buttons[i].setOnClickListener(this);
                buttons[i].setOnLongClickListener(this);
            }
            if (addPenaltyButtons[i] != null) {
                addPenaltyButtons[i].setOnLongClickListener(this);
            }
            if (subPenaltyButtons[i] != null) {
                subPenaltyButtons[i].setOnLongClickListener(this);
            }
        }
        masterHold = findViewById(R.id.masterHold);
        setPlayerButtons();
        setPlayerButtonTextColour();
        setMasterButtonState();
    }


    private void holdTimer(Player player) {
        if (model.holdTimer(player)) {
            setPlayerButtonState(player);
        }
    }


    /**
     * Perform timer calculations when a jammer button is clicked.
     *
     * <p>Checks jammer status to see if the timer should be set to a value other
     * than 30 seconds.</p>
     *
     * <p>If both jammers enter the box at the same time (between jams) then the
     * timers for both are set to zero seconds.</p>
     *
     * <p>If the second jammer is sent to the box within a penalty of the first
     * jammer then the timer is set to match the penalty time served by the
     * first jammer.</p>
     *
     * <p></p>If a jammer is sent to the box for a second time whilst the other jammer
     * is in the box then they serve a full penalty.</p>
     *
     * <p>See WFTDA rule 6.3.11 for handling to multiple jammer penalties.</p>
     *
     * @param actionJammer the jammer for which this action has been called
     * @param otherJammer the opposing jammer
     */
    private void jammerButtonTasks(Player actionJammer, Player otherJammer) {
        if (model.getTimer(actionJammer).isHeld()) {
            if (model.isJammerInBox(otherJammer)) {
                model.getTimer(actionJammer).setTime(model.getTimer(actionJammer).getRemaining() + model.getTimer(otherJammer).getOriginalElapsed());
                model.getTimer(otherJammer).reset();
            }
        } else {
            model.setJammerInBox(actionJammer, true);
            if (model.isJammerInBox(otherJammer)) {
                if (!model.getTimer(otherJammer).isHeld() && !model.isJammerProbation(actionJammer)) {
                    if (model.getTimer(otherJammer).getOriginalElapsed() == 0) {
                        model.getTimer(actionJammer).setTime(0);
                        model.getTimer(otherJammer).setTime(0);
                    } else {
                        model.getTimer(actionJammer).setTime(model.getTimer(otherJammer).getOriginalElapsed());
                        model.getTimer(otherJammer).reset();
                    }
                }
            }
        }
        model.getTimer(actionJammer).start();
        setPenaltyAdjustButtonPairState(actionJammer);
        setPenaltyAdjustButtonPairState(otherJammer);
    }


    /**
     * Resets all timers and jammer statuses.
     */
    public void masterReset() {
        timer.removeCallbacks(updater);
        model.masterReset();
        setPlayerButtons();
        setMasterButtonState();
    }


    /**
     * Handle master pause/resume button clicks.
     *
     * @param view the parent view
     */
    public void masterHoldClicked(View view) {
        model.togglePaused();
        if (!model.isStopped()) {
            timer.postDelayed(updater, 100L);
        }
        setPlayerButtons();
        setMasterButtonState();
    }


    /**
     * Handle player button clicks.
     *
     * @param view the associated view
     */
    public void onClick(View view) {
        if (view.getId() == R.id.jammer) {
            jammerButtonTasks(JAMMER, ALT_JAMMER);
        } else if (view.getId() == R.id.altJammer) {
            jammerButtonTasks(ALT_JAMMER, JAMMER);
        } else if (view.getId() == R.id.blocker1) {
            blockerButtonTasks(BLOCKER_1);
        } else if (view.getId() == R.id.altBlocker1) {
            blockerButtonTasks(ALT_BLOCKER_1);
        } else if (view.getId() == R.id.blocker2) {
            blockerButtonTasks(BLOCKER_2);
        } else if (view.getId() == R.id.altBlocker2) {
            blockerButtonTasks(ALT_BLOCKER_2);
        } else if (view.getId() == R.id.blocker3) {
            blockerButtonTasks(BLOCKER_3);
        } else if (view.getId() == R.id.altBlocker3) {
            blockerButtonTasks(ALT_BLOCKER_3);
        }
        if (model.isStopped()) {
            model.setStopped(false);
            timer.postDelayed(updater, 100L);
        }
        setPlayerButtons();
        setMasterButtonState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_penalty_timer, menu);
        return true;
    }

    /**
     * Method called when the activity is destroyed.
     * The callbacks must be destroyed here. Failure to do so results
     * in the timers running out too quickly as multiple callbacks are
     * made every tenth of a second.
     */
    @Override
    public void onDestroy() {
        timer.removeCallbacks(updater);
        super.onDestroy();
    }

    /**
     * Handle dialog actions.
     *
     * @param fragment the dialog fragment
     */
    public void onDialogPositiveClick(DialogFragment fragment) {
        Bundle state;
        String name = "";
        //noinspection UnusedAssignment
        boolean altTeam = false;
        state = ((ColourPicker) fragment).getState();
        TeamColour teamColour = TeamColour.valueOf(state.getString("teamColour", TeamColour.WHITE.name()));
        name = state.getString("teamName", "");
        altTeam = state.getBoolean("altTeam", false);
        if (altTeam) {
            model.setAltTeamName(name);
            model.setAltTeamColour(teamColour);
        } else {
            model.setTeamName(name);
            model.setTeamColour(teamColour);
        }
        setTeamDetails();
        model.savePreferences(appPrefs);
    }

    /**
     * Handle the cancel button from the team settings dialog.
     *
     * @param fragment the team settings fragment
     */
    public void onDialogNegativeClick(DialogFragment fragment) {
        colourPicker = null;
    }


    /**
     * Handle player button long clicks.
     *
     * @param view the button pressed
     */
    public boolean onLongClick(View view) {
        if (view.getId() == R.id.jammer) {
            holdTimer(JAMMER);
        } else if (view.getId() == R.id.altJammer) {
            holdTimer(ALT_JAMMER);
        } else if (view.getId() == R.id.blocker1) {
            holdTimer(BLOCKER_1);
        } else if (view.getId() == R.id.altBlocker1) {
            holdTimer(ALT_BLOCKER_1);
        } else if (view.getId() == R.id.blocker2) {
            holdTimer(BLOCKER_2);
        } else if (view.getId() == R.id.altBlocker2) {
            holdTimer(ALT_BLOCKER_2);
        } else if (view.getId() == R.id.blocker3) {
            holdTimer(BLOCKER_3);
        } else if (view.getId() == R.id.altBlocker3) {
            holdTimer(ALT_BLOCKER_3);
        } else if (view.getId() == R.id.jammerAdd) {
            adjustPenalty(JAMMER, 1);
        } else if (view.getId() == R.id.altJammerAdd) {
            adjustPenalty(ALT_JAMMER, 1);
        } else if (view.getId() == R.id.blocker1Add) {
            adjustPenalty(BLOCKER_1, 1);
        } else if (view.getId() == R.id.blocker2Add) {
            adjustPenalty(BLOCKER_2, 1);
        } else if (view.getId() == R.id.blocker3Add) {
            adjustPenalty(BLOCKER_3, 1);
        } else if (view.getId() == R.id.altBlocker1Add) {
            adjustPenalty(ALT_BLOCKER_1, 1);
        } else if (view.getId() == R.id.altBlocker2Add) {
            adjustPenalty(ALT_BLOCKER_2, 1);
        } else if (view.getId() == R.id.altBlocker3Add) {
            adjustPenalty(ALT_BLOCKER_3, 1);
        } else if (view.getId() == R.id.jammerSub) {
            adjustPenalty(JAMMER, -1);
        } else if (view.getId() == R.id.altJammerSub) {
            adjustPenalty(ALT_JAMMER, -1);
        } else if (view.getId() == R.id.blocker1Sub) {
            adjustPenalty(BLOCKER_1, -1);
        } else if (view.getId() == R.id.blocker2Sub) {
            adjustPenalty(BLOCKER_2, -1);
        } else if (view.getId() == R.id.blocker3Sub) {
            adjustPenalty(BLOCKER_3, -1);
        } else if (view.getId() == R.id.altBlocker1Sub) {
            adjustPenalty(ALT_BLOCKER_1, -1);
        } else if (view.getId() == R.id.altBlocker2Sub) {
            adjustPenalty(ALT_BLOCKER_2, -1);
        } else if (view.getId() == R.id.altBlocker3Sub) {
            adjustPenalty(ALT_BLOCKER_3, -1);
        }
        return true;
    }


    /**
     * Handle action bar clicks
     * @param item the menu item clicked
     * @return true if the option is handled
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Bundle args;
        if (item.getItemId() == R.id.fullView) {
            setContentView(R.layout.main);
            model.setCurrentView(ALL);
            setTeamDetails();
            getButtonsForView();
        } else if (item.getItemId() == R.id.jammerView) {
            setContentView(R.layout.jammers);
            model.setCurrentView(JAMMERS);
            setTeamDetails();
            getButtonsForView();
        } else if (item.getItemId() == R.id.blockerView) {
            setContentView(R.layout.blockers);
            model.setCurrentView(BLOCKERS);
            setTeamDetails();
            getButtonsForView();
        } else if (item.getItemId() == R.id.switchTeam) {
            model.swapTeams();
            setTeamDetails();
        } else if (item.getItemId() == R.id.setHomeTeam) {
            args = new Bundle();
            args.putString("teamColour", model.getTeamColour().name());
            args.putString("teamName", model.getTeamName() != null
                    ? model.getTeamName()
                    : getString(R.string.team)
            );
            args.putBoolean("altTeam", false);
            colourPicker = ColourPicker.newInstance(1);
            colourPicker.setArguments(args);
            colourPicker.show(getSupportFragmentManager(), "Home Team Settings");
        } else if (item.getItemId() == R.id.setAltTeam) {
            args = new Bundle();
            args.putString("teamColour", model.getAltTeamColour().name());
            args.putString("teamName", model.getTeamName() != null
                    ? model.getAltTeamName()
                    : getString(R.string.altTeam)
            );
            args.putBoolean("altTeam", true);
            colourPicker = ColourPicker.newInstance(1);
            colourPicker.setArguments(args);
            colourPicker.show(getSupportFragmentManager(), "Alternate Team Settings");
        } else if (item.getItemId() == R.id.timerDirectionOption) {
            model.toggleTimerCountDirection();
            invalidateOptionsMenu();
            if (model.isTimersCountUp()) {
                Toast.makeText(this, R.string.timerDirectionUpToast, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, R.string.timerDirectionDownToast, Toast.LENGTH_LONG).show();
            }
        } else if (item.getItemId() == R.id.instructions) {
            Instructions.newInstance(3).show(getSupportFragmentManager(), "Instructions");
        } else if (item.getItemId() == R.id.aboutApp) {
            AboutApp.newInstance(2).show(getSupportFragmentManager(), "About App");
        } else if (item.getItemId() == R.id.masterReset) {
            masterReset();
        }
        return true;
    }


    /**
     * Sets the penalty menu option text
     */
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem timerDirectionItem = menu.findItem(R.id.timerDirectionOption);
        MenuItem switchTeam = menu.findItem(R.id.switchTeam);
        MenuItem fullView = menu.findItem(R.id.fullView);
        MenuItem viewSubMenu = menu.findItem(R.id.viewSubMenu);
        MenuItem optionsSubMenu = menu.findItem(R.id.optionsSubMenu);
        MenuItem jammerView = menu.findItem(R.id.jammerView);
        MenuItem blockerView = menu.findItem(R.id.blockerView);
        MenuItem setHomeTeam = menu.findItem(R.id.setHomeTeam);
        MenuItem setAltTeam = menu.findItem(R.id.setAltTeam);
        setHomeTeam.setTitle("Set " + model.getTeamName());
        setAltTeam.setTitle("Set " + model.getAltTeamName());
        if (model.isTimersCountUp()) {
            timerDirectionItem.setTitle(R.string.timerDirectionUp);
        } else {
            timerDirectionItem.setTitle(R.string.timerDirectionDown);
        }
        switchTeam.setEnabled(model.isStopped());
        if (model.isStopped()) {
            switch (model.getCurrentView()) {
                case JAMMERS:
                    jammerView.setEnabled(false);
                    fullView.setEnabled(true);
                    blockerView.setEnabled(true);
                    break;
                case BLOCKERS:
                    blockerView.setEnabled(false);
                    fullView.setEnabled(true);
                    jammerView.setEnabled(true);
                    break;
                case ALL:
                default:
                    fullView.setEnabled(false);
                    jammerView.setEnabled(true);
                    blockerView.setEnabled(true);
                    break;
            }
        } else {
            fullView.setEnabled(false);
            jammerView.setEnabled(false);
            blockerView.setEnabled(false);
        }
        viewSubMenu.setEnabled(true);
        optionsSubMenu.setEnabled(true);
        return true;
    }


    /**
     * Restore variables following a device configuration change.
     * (This is most likely an orientation change.)
     * Note that an immediate call is made to the updater. The accuracy of
     * the timer is affected by this, shortening the timed period by up to
     * a tenth of a second each time a configuration change occurs.
     * @param state the bundle of variables
     */
    @Override
    public void onRestoreInstanceState(@NonNull Bundle state) {
        model.restoreState(state);
        if (model.isPaused()) {
            setPlayerButtons();
            setMasterButtonState();
        }
        setTeamDetails();
        timer.post(updater);
    }


    /**
     * Save state before device configuration change. Android will handle
     * all views, so we just deal with internal variables.
     * @param state the bundle in which to save variable states
     */
    @Override
    public void onSaveInstanceState(@NonNull Bundle state) {
        model.saveState(state);
        super.onSaveInstanceState(state);
    }


    @Override
    public void onStop() {
        super.onStop();
        model.savePreferences(appPrefs);;
    }


    /**
     * Sets the state of the master pause/resume button.
     */
    private void setMasterButtonState() {
        if (model.isStopped()) {
            if (model.isPaused()) {
                masterHold.setText(R.string.masterHoldHeld, TextView.BufferType.NORMAL);
            } else {
                masterHold.setText(R.string.masterHoldFree, TextView.BufferType.NORMAL);
            }
        } else {
            if (model.isPaused()) {
                masterHold.setText(R.string.masterResume, TextView.BufferType.NORMAL);
            } else {
                masterHold.setText(R.string.masterPause, TextView.BufferType.NORMAL);
            }
        }
    }


    /**
     * Set the state of the given penalty button add/subtract pair based on counter state
     * @param player the player role.
     */
    private void setPenaltyAdjustButtonPairState(Player player) {
        int ordinal = player.ordinal();
        if (addPenaltyButtons[ordinal] == null) {
            return;
        }
        int disabledColour;
        if (player.homeTeam) {
            // home team
            disabledColour = model.getTeamContrastTextColour();
        } else {
            disabledColour = model.getAltTeamContrastTextColour();
        }
        if (model.isStopped()
                || !model.getTimer(player).isRunning()
                || (model.getTimer(player).isRunning()
                        && model.getTimer(player).getRemaining() == 0)) {
            addPenaltyButtons[ordinal].setLongClickable(false);
            addPenaltyButtons[ordinal].setTextColor(disabledColour);
            subPenaltyButtons[ordinal].setLongClickable(false);
            subPenaltyButtons[ordinal].setTextColor(disabledColour);
        } else {
            if (model.getTimer(player).getAdditionals() < 6) {
                addPenaltyButtons[ordinal].setLongClickable(true);
                if (player.homeTeam) {
                    addPenaltyButtons[ordinal].setTextColor(model.getTeamTextColour());
                } else {
                    addPenaltyButtons[ordinal].setTextColor(model.getAltTeamTextColour());
                }
            } else {
                addPenaltyButtons[ordinal].setLongClickable(false);
                addPenaltyButtons[ordinal].setTextColor(disabledColour);
            }
            if (model.getTimer(player).getAdditionals() > 0
                    && model.getTimer(player).getRemaining() >= model.getPenaltyLength()) {
                subPenaltyButtons[ordinal].setLongClickable(true);
                if (player.homeTeam) {
                    subPenaltyButtons[ordinal].setTextColor(model.getTeamTextColour());
                } else {
                    subPenaltyButtons[ordinal].setTextColor(model.getAltTeamTextColour());
                }
            } else {
                subPenaltyButtons[ordinal].setLongClickable(false);
                subPenaltyButtons[ordinal].setTextColor(disabledColour);
            }
        }
    }


    /**
     * Sets the main player button state for the given player.
     * <p>Note! We could enable/disable longClick but if it's disabled then a longClick
     * is interpreted as a regular click. This can lead to unexpected behaviour.
     * Better that the longClick is handled but no action is taken.</p>
     *
     * @param player the skater role
     */
    private void setPlayerButtonState(Player player) {
        int ordinal = player.ordinal();
        if (buttons[ordinal] == null) {
            return;
        }
        if (model.isStopped() || !model.getTimer(player).isRunning()) {
            SpannableStringBuilder builder = new SpannableStringBuilder();
            if (player.jammer) {
                builder.append(getString(R.string.jammer));
            } else {
                builder.append(getString(R.string.blocker));
            }
            buttons[ordinal].setTextSize(18);
            builder.setSpan(new RelativeSizeSpan(1f), 0, builder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            buttons[ordinal].setText(builder, TextView.BufferType.SPANNABLE);
        } else {
            if (model.getTimer(player).isRunning()) {
                buttons[ordinal].setTextSize(36);
                if (model.isTimersCountUp()) {
                    buttons[ordinal].setText(renderTimerForButton(player), TextView.BufferType.SPANNABLE);
                } else {
                    buttons[ordinal].setText(renderTimerForButton(player), TextView.BufferType.SPANNABLE);
                }
            }
        }
    }


    private SpannableStringBuilder renderTimerForButton(Player player) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        if (model.isTimersCountUp()) {
            builder.append(model.getTimer(player).getDisplayRunTime());
            int length = builder.length();
            builder.append("\n(");
            builder.append(model.getTimer(player).getDisplayTime().replaceAll("[\\[\\]]", ""));
            builder.append(")");
            builder.setSpan(new RelativeSizeSpan(1f), 0, length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.setSpan(new ForegroundColorSpan(Color.GRAY), length, builder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            builder.setSpan(new RelativeSizeSpan(0.4f), length, builder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            builder.append(model.getTimer(player).getDisplayTime());
            builder.setSpan(new RelativeSizeSpan(1f), 0, builder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return builder;
    }


    /**
     * Sets button state depending on activity/timer state.
     */
    private void setPlayerButtons() {
        Arrays.stream(Player.values()).forEach(player -> {
            setPlayerButtonState(player);
            setPenaltyAdjustButtonPairState(player);
        });
    }


    /**
     * Sets the text colour of the main penalty buttons to contrast the team colour.
     */
    private void setPlayerButtonTextColour() {
        for (int i = 0; i < model.getTimerCount(); i++) {
            if (buttons[i] != null) {
                if (i % 2 == 0) {
                    buttons[i].setTextColor(model.getTeamTextColour());
                } else {
                    buttons[i].setTextColor(model.getAltTeamTextColour());
                }
            }
        }
    }


    private void setTeamDetails() {
        (findViewById(R.id.team)).setBackgroundColor(model.getTeamBackgroundColor());
        ((TextView)findViewById(R.id.teamName)).setText(model.getTeamName(), TextView.BufferType.NORMAL);
        ((TextView)findViewById(R.id.teamName)).setTextColor(model.getTeamTextColour());
        if (model.getCurrentView() != BLOCKERS) {
            (findViewById(R.id.altTeam)).setBackgroundColor(model.getAltTeamBackgroundColour());
            ((TextView)findViewById(R.id.altTeamName)).setText(model.getAltTeamName(), TextView.BufferType.NORMAL);
            ((TextView)findViewById(R.id.altTeamName)).setTextColor(model.getAltTeamTextColour());
        }
        setPlayerButtons();
        setPlayerButtonTextColour();
    }


    /**
     * A Runnable class required to handle callbacks.
     *
     * <p>Callbacks are made every tenth of a second.
     * <br>
     * If the timers are 'stopped' then no further action is taken.
     * Otherwise, each of the CountdownTimers in the array is reviewed.
     * The usual action is to decrement the count by one.
     * <br>
     * Button titles are reset when timers run out.
     * Jammer status is reviewed.
     * <br>
     * Once all timers have run out 'stopped' state is set.</p>
     */
    class Updater implements Runnable {

        /** Run method */
        public void run() {
            int timerCount = 0;
            if (!model.isPaused()) {
                for (int i = 0; i < buttons.length; i++) {
                    Player player = Player.values()[i];
                    if (model.getTimer(player).isRunning()) {
                        timerCount++;
                        if (model.getTimer(player).getRemaining() == 100) {
                            vibe.vibrate(vibeShort);
                        }
                        if (buttons[i] != null) {
                            if (model.isTimersCountUp()) {
                                buttons[i].setText(renderTimerForButton(player), TextView.BufferType.SPANNABLE);
                            } else {
                                buttons[i].setText(renderTimerForButton(player), TextView.BufferType.SPANNABLE);
                            }
                            buttons[i].setTextSize(36);
                        }
                        model.getTimer(player).decrement();
                        if (model.getTimer(player).getRemaining() == model.getPenaltyLength() - 1) {
                            setPenaltyAdjustButtonPairState(player);
                        }
                        if (model.getTimer(player).getRemaining() == 0) {
                            vibe.vibrate(vibeLong);
                            setPenaltyAdjustButtonPairState(player);
                        }
                    } else {
                        if (buttons[i] != null) {
                            buttons[i].setTextSize(18);
                        }
                        if (player.jammer) {
                            if (buttons[i] != null) {
                                buttons[i].setText(R.string.jammer, TextView.BufferType.NORMAL);
                            }
                            if (i == JAMMER.ordinal() && model.isJammerInBox(JAMMER)) {
                                model.setJammerInBox(JAMMER, false);
                                model.setJammerProbation(ALT_JAMMER, false);
                                if (model.isJammerInBox(ALT_JAMMER)) {
                                    model.setJammerProbation(JAMMER, true);
                                }
                            }
                            if (i == ALT_JAMMER.ordinal() && model.isJammerInBox(ALT_JAMMER)) {
                                model.setJammerInBox(ALT_JAMMER, false);
                                model.setJammerProbation(JAMMER, false);
                                if (model.isJammerInBox(JAMMER)) {
                                    model.setJammerProbation(ALT_JAMMER, true);
                                }
                            }
                        } else {
                            if (buttons[i] != null) {
                                buttons[i].setText(R.string.blocker, TextView.BufferType.NORMAL);
                            }
                        }
                    }
                }
                if (timerCount > 0) {
                    timer.postDelayed(updater, 100L);
                } else {
                    model.setStopped(true);
                    model.setPaused(false);
                    setPlayerButtons();
                    setMasterButtonState();
                }
            }
        }
    }
}
