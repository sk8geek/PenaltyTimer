package uk.co.channele.timer;

public enum Player {
    JAMMER(true, true),
    ALT_JAMMER(true, false),
    BLOCKER_1(false, true),
    ALT_BLOCKER_1(false, false),
    BLOCKER_2(false, true),
    ALT_BLOCKER_2(false, false),
    BLOCKER_3(false, true),
    ALT_BLOCKER_3(false, false);


    public final boolean jammer;
    public final boolean homeTeam;

    Player(boolean jammer, boolean homeTeam) {
        this.jammer = jammer;
        this.homeTeam = homeTeam;
    }
}
