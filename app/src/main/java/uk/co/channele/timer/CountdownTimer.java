package uk.co.channele.timer;

import java.io.Serializable;

/**
 * A countdown timer class.
 *
 * <p>Note that these are not real timers, simply counters. Timing is implemented
 * through the use of a Handler in the main class.</p>
 * <p>Timing is in tenths of a second.</p>
 */
public class CountdownTimer implements Serializable {

    private int resetValue;
    private int additionals = 0;
    private int remaining;
    private int runTime = 0;
    private boolean running = false;
    private boolean hold = false;


    CountdownTimer(int resetTo) {
        resetValue = resetTo;
        remaining = resetValue;
    }

    /**
     * Add a penalty to the counter. The total number of penalties must not exceed 7.
     */
    void addPenalty() {
        if (additionals < 6) {
            remaining = remaining + resetValue;
            additionals++;
        }
    }

    /**
     * Mark the timer as running.
     */
    void start() {
        hold = false;
        running = true;
    }

    /**
     * Decrement the counter, or reset it if it times out.
     */
    void decrement() {
        if (!hold) {
            if (remaining > 0) {
                remaining--;
                runTime++;
            } else {
                reset();
            }
        }
    }

    /**
     * Reset the counter.
     */
    void reset() {
        remaining = resetValue;
        runTime = 0;
        running = false;
        hold = false;
        additionals = 0;
    }

    /**
     * Test to see if the counter is held
     * @return true is paused and time remaining
     */
    boolean isHeld() {
        return hold;
    }

    /**
     * Test if the counter is running.
     * @return true if running
     */
    boolean isRunning() {
        return running;
    }

    int getAdditionals() {
        return additionals;
    }

    /**
     * Get the time, in seconds, that the timer has to run.
     * The value is rounded down.
     * @return the number of seconds remaining as a String
     */
    String getDisplayTime() {
        float dividable = (float)remaining;
        if (hold) {
            return ("[" + Float.toString(dividable / 10) + "]");
        }
        return Float.toString(dividable / 10);
    }


    /**
     * Get the time, in seconds, that the timer has been running. The value is rounded down.
     * <p>Added in code version 17 at the request of a user</p>
     * @return seconds that the timer has been running as a String
     */
    String getDisplayRunTime() {
        float dividable = (float)runTime;
        if (hold) {
            return ("[" + Float.toString(dividable / 10) + "]");
        }
        return (Float.toString(dividable / 10));
    }


    /**
     * Get the time remaining, in tenths of a second.
     * @return time remaining in tenths of a second
     */
    int getRemaining() {
        return remaining;
    }


    /**
     * Get the time elapsed, in tenths of a second.
     * @return time elapsed in tenths of a second
     */
    int getOriginalElapsed() {
        return resetValue - remaining - (resetValue * additionals);
    }

    /**
     * Hold/release the counter
     * @param hold true to hold the counter, false to release it if time remains
     */
    void setHold(boolean hold) {
        this.hold = hold;
    }

    /**
     * Set the remaining time, in tenths of a second
     * @param time new time remaining in tenths of a second
     */
    void setTime(int time) {
        remaining = time;
    }

    /**
     * Subtract a penalty from the counter, if at least a penalty is left on
     * the counter.
     */
    void subtractPenalty() {
        if (additionals > 0) {
            remaining = remaining - resetValue;
            additionals--;
            if (remaining == 0) {
                reset();
            }
        }
    }

}
