package uk.co.channele.timer;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;
import static uk.co.channele.timer.Player.*;
import static uk.co.channele.timer.PlayerView.*;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.lifecycle.ViewModel;

public class TimerViewModel extends ViewModel {

    private int penaltyLength = 300;
    private boolean timersCountUp = false;
    private String[] colours = null;
    private boolean stopped = true;
    private boolean paused = false;
    private boolean[] jammerInBox = { false, false };
    private boolean[] jammerProbation = { false, false };

    private String teamName = null;
    private TeamColour teamColour = TeamColour.WHITE;
    private int teamBackgroundColour = WHITE;
    private int teamTextColour = BLACK;

    private String altTeamName = null;
    private TeamColour altTeamColour = TeamColour.BLACK;
    private int altTeamBackgroundColour = BLACK;
    private int altTeamTextColour = WHITE;

    private PlayerView currentView = ALL;

    private final CountdownTimer[] timers = {
            new CountdownTimer(penaltyLength),
            new CountdownTimer(penaltyLength),
            new CountdownTimer(penaltyLength),
            new CountdownTimer(penaltyLength),
            new CountdownTimer(penaltyLength),
            new CountdownTimer(penaltyLength),
            new CountdownTimer(penaltyLength),
            new CountdownTimer(penaltyLength)
    };


    public TimerViewModel() {
    }


    PlayerView getCurrentView() {
        return currentView;
    }

    void setCurrentView(PlayerView view) {
        this.currentView = view;
    }


    boolean isTimersCountUp() {
        return timersCountUp;
    }

    void toggleTimerCountDirection() {
        timersCountUp = !timersCountUp;
    }


    void setStopped(boolean state) {
        stopped = state;
    }

    boolean isStopped() {
        return stopped;
    }


    void setPaused(boolean state) {
        paused = state;
    }

    boolean isPaused() {
        return paused;
    }

    void togglePaused() {
        paused = !paused;
    }



    void setJammerInBox(Player jammer, boolean state) {
        jammerInBox[jammer.ordinal()] = state;
    }

    boolean isJammerInBox(Player jammer) {
        return jammerInBox[jammer.ordinal()];
    }



    void setJammerProbation(Player jammer, boolean state) {
        jammerProbation[jammer.ordinal()] = state;
    }

    boolean isJammerProbation(Player jammer) {
        return jammerProbation[jammer.ordinal()];
    }


    TeamColour getTeamColour() {
        return teamColour;
    }

    void setTeamColour(TeamColour teamColour) {
        this.teamColour = teamColour;
        teamBackgroundColour = Color.parseColor(colours[teamColour.ordinal()]);
        teamTextColour = getContrastingColour(this.teamBackgroundColour);
    }

    TeamColour getAltTeamColour() {
        return altTeamColour;
    }

    void setAltTeamColour(TeamColour altTeamColour) {
        this.altTeamColour = altTeamColour;
        altTeamBackgroundColour = Color.parseColor(colours[altTeamColour.ordinal()]);
        altTeamTextColour = getContrastingColour(this.altTeamBackgroundColour);
    }

    void setColours(String[] colours) {
        this.colours = colours;
    }

    int getTeamBackgroundColor() {
        return teamBackgroundColour;
    }

    int getAltTeamBackgroundColour() {
        return altTeamBackgroundColour;
    }

    int getTeamTextColour() {
        return teamTextColour;
    }

    int getAltTeamTextColour() {
        return altTeamTextColour;
    }


    String getTeamName() {
        return teamName;
    }

    void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    String getAltTeamName() {
        return altTeamName;
    }

    void setAltTeamName(String altTeamName) {
        this.altTeamName = altTeamName;
    }


    void swapTeams() {
        String swapTeamName = teamName;
        teamName = altTeamName;
        altTeamName = swapTeamName;
        TeamColour swapColour = teamColour;
        setTeamColour(altTeamColour);
        setAltTeamColour(swapColour);
    }


    int getPenaltyLength() {
        return penaltyLength;
    }


    int getTimerCount() {
        return timers.length;
    }

    CountdownTimer getTimer(Player player) {
        return timers[player.ordinal()];
    }


    boolean holdTimer(Player player) {
        if (timers[player.ordinal()].isRunning() && !timers[player.ordinal()].isHeld()) {
            timers[player.ordinal()].setHold(true);
            return true;
        }
        return false;
    }

    private void resetTimers() {
        for (CountdownTimer timer : timers) {
            timer.reset();
        }
    }

    void masterReset() {
        stopped = true;
        paused = false;
        jammerInBox[JAMMER.ordinal()] = false;
        jammerInBox[ALT_JAMMER.ordinal()] = false;
        jammerProbation[JAMMER.ordinal()] = false;
        jammerProbation[ALT_JAMMER.ordinal()] = false;
        resetTimers();
    }


    /**
     * Retrieve application preferences
     */
    void retrievePreferences(SharedPreferences prefs) {
        setTeamColour(TeamColour.valueOf(prefs.getString("teamColour", TeamColour.WHITE.name())));
        setAltTeamColour(TeamColour.valueOf(prefs.getString("altTeamColour", TeamColour.BLACK.name())));
        teamName = prefs.getString("teamName", teamName);
        altTeamName = prefs.getString("altTeamName", altTeamName);
        penaltyLength = prefs.getInt("penaltyLength", penaltyLength);
        timersCountUp = prefs.getBoolean("timersCountUp", false);
        try {
            currentView = PlayerView.valueOf(prefs.getString("currentView", "ALL"));
        } catch (ClassCastException e) {
            currentView = ALL;
        }
    }

    /**
     * Save application preferences
     */
    void savePreferences(SharedPreferences prefs) {
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putString("teamColour", teamColour.name());
        prefsEditor.putString("altTeamColour", altTeamColour.name());
        prefsEditor.putString("teamName", teamName);
        prefsEditor.putString("altTeamName", altTeamName);
        prefsEditor.putInt("penaltyLength", penaltyLength);
        prefsEditor.putBoolean("timersCountUp", timersCountUp);
        prefsEditor.putString("currentView", currentView.name());
        prefsEditor.apply();
    }


    void saveState(Bundle state) {
        state.putString("teamColour", teamColour.name());
        state.putString("altTeamColour", altTeamColour.name());
        state.putString("teamName", teamName);
        state.putString("altTeamName", altTeamName);
        state.putBoolean("stopped", stopped);
        state.putBoolean("paused", paused);
        state.putBooleanArray("jammerInBox", jammerInBox);
        state.putBooleanArray("jammerProbation", jammerProbation);
        state.putString("currentView", currentView.name());
        for (int i = 0; i < timers.length; i++ ) {
            state.putSerializable("timer" + i, timers[i]);
        }
    }

    void restoreState(Bundle state) {
        setTeamColour(TeamColour.valueOf(state.getString("teamColour")));
        setAltTeamColour(TeamColour.valueOf(state.getString("altTeamColour")));
        if (state.containsKey("teamName")) {
            teamName = state.getString("teamName");
        }
        if (state.containsKey("altTeamName")) {
            altTeamName = state.getString("altTeamName");
        }
        jammerInBox = state.getBooleanArray("jammerInBox");
        jammerProbation = state.getBooleanArray("jammerProbation");
        currentView = PlayerView.valueOf(state.getString("currentView"));
        for (int i = 0; i < timers.length; i++ ) {
            if (state.containsKey("timer" + i)) {
                timers[i] = (CountdownTimer)state.getSerializable("timer" + i);
            }
        }
        if (state.containsKey("stopped")) {
            stopped = state.getBoolean("stopped");
            if (state.containsKey("paused")) {
                paused = state.getBoolean("paused");
            }
        }
    }


    int getTeamContrastTextColour() {
        if (getContrastingColour(teamBackgroundColour) == WHITE) {
            return Color.DKGRAY;
        }
        return Color.LTGRAY;
    }

    int getAltTeamContrastTextColour() {
        if (getContrastingColour(altTeamBackgroundColour) == WHITE) {
            return Color.DKGRAY;
        }
        return Color.LTGRAY;
    }

    /**
     * Gets the contrasting colour to that given (for text)
     * @param background the colour of the background
     * @return the contrasting colour, either black or white
     */
    static int getContrastingColour(int background) {
        int mean = (Color.red(background) + Color.green(background) + Color.blue(background)) / 3;
        if (mean < 128) {
            return WHITE;
        } else {
            return BLACK;
        }
    }


}
