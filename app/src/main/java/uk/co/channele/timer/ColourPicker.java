/* 
 * @(#) ColourPicker.java
 * 
 * Copyright 2013 to 2018 Steven J Lilley
 *
 * A dialog fragment to provide team settings.
 *
 * This file is part of PenaltyTimer.
 *
 * PenaltyTimer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * stevenjlilley@gmail.com
 */
package uk.co.channele.timer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import org.jetbrains.annotations.NotNull;
import static uk.co.channele.timer.TeamColour.*;

public class ColourPicker extends DialogFragment implements OnClickListener {

	private final Button[] buttons = new Button[9];
	private EditText teamNameEditor;
	private TeamColour teamColour = WHITE;
	private boolean altTeam = false;
	private String teamName;
	private SettingsListener settingsListener;

	public interface SettingsListener {
		void onDialogPositiveClick(DialogFragment dialog);
		@SuppressWarnings("UnusedDeclaration")
		void onDialogNegativeClick(DialogFragment dialog);
	}

	@SuppressWarnings("UnusedParameters")
	public static ColourPicker newInstance(int id) {
		return new ColourPicker();
	}

	@Override
	public void onAttach(@NotNull Activity activity) {
		super.onAttach(activity);
		try {
			settingsListener = (SettingsListener)activity;
		} catch (ClassCastException ccex) {
			throw new ClassCastException(activity.toString() + " doesn't implement SettingsListener");
		}
	}

	@NotNull
	@Override
	public Dialog onCreateDialog(Bundle state) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		Dialog dialog;
		View view = inflater.inflate(R.layout.colourpicker, null);
		builder.setView(view);
		builder.setPositiveButton(R.string.select, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				settingsListener.onDialogPositiveClick(ColourPicker.this);
			}
		});
		builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int id) {
				ColourPicker.this.getDialog().cancel();
			}
		});
//		dialog = builder.create();
		buttons[0] = (Button)view.findViewById(R.id.white);
		buttons[1] = (Button)view.findViewById(R.id.yellow);
		buttons[2] = (Button)view.findViewById(R.id.orange);
		buttons[3] = (Button)view.findViewById(R.id.red);
		buttons[4] = (Button)view.findViewById(R.id.pink);
		buttons[5] = (Button)view.findViewById(R.id.purple);
		buttons[6] = (Button)view.findViewById(R.id.blue);
		buttons[7] = (Button)view.findViewById(R.id.green);
		buttons[8] = (Button)view.findViewById(R.id.black);
		for (Button button : buttons) {
			button.setOnClickListener(this);
		}
		buttons[teamColour.ordinal()].setText("X", TextView.BufferType.NORMAL);
		teamNameEditor = (EditText)view.findViewById(R.id.teamName);
		teamNameEditor.setText(teamName, TextView.BufferType.EDITABLE);
		dialog = builder.create();
		dialog.setTitle(getString(R.string.colourPicker));
		return dialog;
	}

	public void onClick(View view) {
		int ordinal = teamColour.ordinal();
		buttons[ordinal].setText(" ", TextView.BufferType.NORMAL);
		if (view.getId() == R.id.white) {
			teamColour = WHITE;
		} else if (view.getId() == R.id.yellow) {
			teamColour = YELLOW;
		} else if (view.getId() == R.id.orange) {
			teamColour = ORANGE;
		} else if (view.getId() == R.id.red) {
			teamColour = RED;
		} else if (view.getId() == R.id.pink) {
			teamColour = PINK;
		} else if (view.getId() == R.id.purple) {
			teamColour = PURPLE;
		} else if (view.getId() == R.id.blue) {
			teamColour = BLUE;
		} else if (view.getId() == R.id.green) {
			teamColour = GREEN;
		} else if (view.getId() == R.id.black) {
			teamColour = BLACK;
		}
		buttons[teamColour.ordinal()].setText("X", TextView.BufferType.NORMAL);
	}

	/**
	 * @param state state bundle
	 */
	@Override
	public void onSaveInstanceState(Bundle state) {
		state.putString("teamColour", teamColour.name());
		state.putString("teamName", teamName);
		super.onSaveInstanceState(state);
	}

	/**
	 * @param args set values
	 */
	@Override
	public void setArguments(Bundle args) {
		if (args != null) {
			if (args.containsKey("teamColour")) {
				teamColour = TeamColour.valueOf(args.getString("teamColour"));
			}
			if (args.containsKey("teamName")) {
				teamName = args.getString("teamName");
			}
			altTeam = args.getBoolean("altTeam");
		}
	}

	public Bundle getState() {
		Bundle state = new Bundle();
		state.putString("teamColour", teamColour.name());
		state.putString("teamName", teamNameEditor.getText().toString());
		state.putBoolean("altTeam", altTeam);
		return state;
	}

}
